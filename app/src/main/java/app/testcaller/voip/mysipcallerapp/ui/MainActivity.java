package app.testcaller.voip.mysipcallerapp.ui;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.csipsimple.api.ISipService;
import com.csipsimple.api.SipManager;
import com.csipsimple.api.SipProfile;
import com.csipsimple.api.SipProfileState;
import com.csipsimple.service.SipService;
import com.csipsimple.ui.account.AccountsEditList;
import com.csipsimple.utils.Log;

import java.io.IOException;
import java.util.ArrayList;

import app.testcaller.voip.mysipcallerapp.AccountHelper;
import app.testcaller.voip.mysipcallerapp.ConfigHelper;
import app.testcaller.voip.mysipcallerapp.R;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    private static final String THIS_FILE = "MainActivity";
    private ISipService service;
    private Button call;
    private TextView info;

    private Handler h = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        checkSipPermission();

        info = (TextView) findViewById(R.id.infoTextView);
        call = (Button) findViewById(R.id.buttonCall);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call();
            }
        });

        ConfigHelper.applyPrefs();


        Intent serviceIntent = new Intent(MainActivity.this, SipService.class);
        // Optional, but here we bundle so just ensure we are using csipsimple package
        serviceIntent.setPackage(MainActivity.this.getPackageName());
        serviceIntent.putExtra(SipManager.EXTRA_OUTGOING_ACTIVITY, new ComponentName(MainActivity.this, MainActivity.class));
        Timber.d("Starting service");
        startService(serviceIntent);

        bindService(serviceIntent, new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName arg0, IBinder arg1) {
                service = ISipService.Stub.asInterface(arg1);
                call.setEnabled(true);

                if (!AccountHelper.haveStoredAccount(MainActivity.this)) {
                    SipProfile account = AccountHelper.getAccount(MainActivity.this);
                    account.setCallerId("1111");
                    AccountHelper.saveAccount(MainActivity.this, account);
                }

            /*
             * timings.addSplit("Service connected"); if(configurationService !=
             * null) { timings.dumpToLog(); }
             */
            }

            @Override
            public void onServiceDisconnected(ComponentName arg0) {
                service = null;
            }
        }, Context.BIND_AUTO_CREATE);

        getContentResolver().registerContentObserver(SipProfile.ACCOUNT_STATUS_URI, true, mObserver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        call = null;
        service = null;
        getContentResolver().unregisterContentObserver(mObserver);
    }

    ContentObserver mObserver = new ContentObserver(h) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            updateRegState();
        }
    };


    private void call() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Title");
        pd.setMessage("Message");
        // меняем стиль на индикатор
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // включаем анимацию ожидания
        pd.setIndeterminate(true);
        pd.show();

        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url("http://193.124.115.122/Mainservice.svc/DebugWaitCall")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                Timber.e("request failed = %s", e);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, String.format("Calling RESTAPI failed, %s", e), Toast.LENGTH_SHORT).show();
                    }
                });
                pd.dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Timber.d("Response = %s", response);
                String uuid = response.body().string().replace("\"", "");
                SipProfile account = AccountHelper.getAccount(MainActivity.this);
                account.setCallerId(uuid);
                AccountHelper.saveAccount(MainActivity.this, account);
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        placeCallWithOption(null);
                        pd.dismiss();
                    }
                }, 3000);
            }
        });
    }

    private void placeCallWithOption(Bundle b) {
        if (service == null) {
            return;
        }

        String toCall = "";
        // Find account to use
        SipProfile acc = AccountHelper.getAccount(this.getBaseContext());
        if (acc == null) {
            return;
        }

        // Find number to dial
        toCall = "911";

        if (TextUtils.isEmpty(toCall)) {
            return;
        }


        // -- MAKE THE CALL --//

        // It is a SIP account, try to call service for that
        try {
            service.makeCallWithOptions(toCall, (int) acc.id, b);
        } catch (RemoteException e) {
            Log.e(THIS_FILE, "Service can't be called to make the call");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.account:
                startActivity(new Intent(this, AccountsEditList.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateRegState() {
        Log.d(THIS_FILE, "Update registration state");
        ArrayList<SipProfileState> activeProfilesState = new ArrayList<SipProfileState>();
        Cursor c = getContentResolver().query(SipProfile.ACCOUNT_STATUS_URI, null, null, null, null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    c.moveToFirst();
                    do {
                        SipProfileState ps = new SipProfileState(c);
                        activeProfilesState.add(ps);
                    } while (c.moveToNext());
                }
            } catch (Exception e) {
                Log.e(THIS_FILE, "Error on looping over sip profiles", e);
            } finally {
                c.close();
            }
        }
        if (!activeProfilesState.isEmpty()) {
            SipProfileState sipProfileState = activeProfilesState.get(0);
            String status = "";
            switch (sipProfileState.getStatusCode()){
                case -1:
                    status="Подкоючения";
                    break;
                case 200:
                    status = "Подключенно";
                    break;
                default:
                    status = "Ошибка";
                    break;
            }
            Timber.d(sipProfileState.toString());
            info.setText(status+'\n'+sipProfileState.getStatusText()+'\n'+sipProfileState.getStatusCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateRegState();
    }
}
