package app.testcaller.voip.mysipcallerapp;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.csipsimple.api.SipProfile;
import com.csipsimple.api.SipUri;
import com.csipsimple.db.DBProvider;
import com.csipsimple.utils.Log;

import timber.log.Timber;

/**
 * Created by Beka on 5/31/16.
 */
public class AccountHelper {


    private static final String THIS_FILE = "AccountHelper";
    private static final String server = "188.35.188.34:5060";

    public static SipProfile getAccount(Context context) {
        SipProfile account = null;
        Cursor c = context.getContentResolver().query(SipProfile.ACCOUNT_URI, DBProvider.ACCOUNT_FULL_PROJECTION, null, null, null);
        if (c != null) {
            try {
                if (c.moveToFirst()) {
                    do {
                        account = new SipProfile(c);
                    } while (c.moveToNext());
                }
            } catch (Exception e) {
                Log.e(THIS_FILE, "Error on looping over sip profiles", e);
            } finally {
                c.close();
            }
        }

        if (account == null) {
            Log.d(THIS_FILE, "begin of save ....");
            account = new SipProfile();

            account.display_name = "1901";
            String[] serverParts = server.split(":");
            account.acc_id = "1111" +
                    " <sip:" + SipUri.encodeUser("1901".trim()) + "@" + serverParts[0].trim() + ">";

            account.reg_uri = "sip:" + server;

            account.realm = "*";

            account.username = "1901";
            account.data = "VIsrTWZOR6oW5y2";
            account.scheme = SipProfile.CRED_SCHEME_DIGEST;
            account.datatype = SipProfile.CRED_DATA_PLAIN_PASSWD;

            account.transport = SipProfile.TRANSPORT_AUTO;

        }
        Timber.d("Read account : %s", account);
        return account;
    }

    public static boolean haveStoredAccount(Context context) {
        SipProfile account = null;
        Cursor c = context.getContentResolver().query(SipProfile.ACCOUNT_URI, DBProvider.ACCOUNT_FULL_PROJECTION, null, null, null);
        if (c != null) {
            try {
                if (c.moveToFirst()) {
                    do {
                        account = new SipProfile(c);
                    } while (c.moveToNext());
                }
            } catch (Exception e) {
                Log.e(THIS_FILE, "Error on looping over sip profiles", e);
            } finally {
                c.close();
            }
        }

        return account != null;
    }

    public static void saveAccount(Context context, SipProfile account) {
        Timber.d("Write account : %s", account);
        if (account == null || account.id == SipProfile.INVALID_ID) {
            // This account does not exists yet
            Uri uri = context.getContentResolver().insert(SipProfile.ACCOUNT_URI, account.getDbContentValues());
        } else {
            context.getContentResolver().update(ContentUris.withAppendedId(SipProfile.ACCOUNT_ID_URI_BASE, account.id), account.getDbContentValues(), null, null);
        }
    }
}