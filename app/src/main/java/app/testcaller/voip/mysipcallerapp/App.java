package app.testcaller.voip.mysipcallerapp;

import android.app.Application;
import android.content.Context;

import timber.log.Timber;

/**
 * Created by Beka on 5/30/16.
 */
public class App extends Application {

    private static Context sContext;
    @Override
    public void onCreate() {
        super.onCreate();
        sContext=this;
        Timber.plant(new Timber.DebugTree());
    }

    public static Context getContext() {
        return sContext;
    }
}
