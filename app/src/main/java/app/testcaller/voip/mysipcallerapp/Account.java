package app.testcaller.voip.mysipcallerapp;

import android.support.annotation.ColorInt;

import java.io.Serializable;

/**
 * Created by Beka on 5/28/16.
 */
public class Account implements Serializable {
    private @ColorInt int colorId;
    private int accountStatusId;
    private String accountName;
    private String callerId;
    private String userName;
    private String domain;
    private String password;

    public Account(String accountName, String callerId, String userName, String domain, String password) {
        this.accountName = accountName;
        this.callerId = callerId;
        this.userName = userName;
        this.domain = domain;
        this.password = password;
    }

    public Account() {
    }

    public String getAccountName() {
        return accountName;
    }

    public String getCallerId() {
        return callerId;
    }

    public String getUserName() {
        return userName;
    }

    public String getDomain() {
        return domain;
    }

    public String getPassword() {
        return password;
    }

    public int getColorId() {
        return colorId;
    }

    public int getAccountStatusId() {
        return accountStatusId;
    }
}
