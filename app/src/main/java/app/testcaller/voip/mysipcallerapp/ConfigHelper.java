package app.testcaller.voip.mysipcallerapp;

import com.csipsimple.api.SipConfigManager;
import com.csipsimple.utils.PreferencesWrapper;

/**
 * Created by Beka on 6/4/16.
 */
public class ConfigHelper {
    public static void applyPrefs() {
        boolean integrate = false;
        boolean useGsm = true;

        // About integration
        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.INTEGRATE_WITH_DIALER, integrate);
        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.INTEGRATE_WITH_CALLLOGS, integrate);


        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.USE_3G_IN, (useGsm && false));
        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.USE_3G_OUT, useGsm);
        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.USE_GPRS_IN, (useGsm && false));
        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.USE_GPRS_OUT, useGsm);
        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.USE_EDGE_IN, (useGsm && false));
        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.USE_EDGE_OUT, useGsm);

        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.USE_WIFI_IN, false);
        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.USE_WIFI_OUT, true);

        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.USE_OTHER_IN, false);
        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.USE_OTHER_OUT, true);

        SipConfigManager.setPreferenceBooleanValue(App.getContext(), SipConfigManager.LOCK_WIFI, (false) && !useGsm);


        SipConfigManager.setPreferenceBooleanValue(App.getContext(), PreferencesWrapper.HAS_ALREADY_SETUP, true);
    }

}
